//
//  ViewController.swift
//  CoreML-Model-Comparer
//
//  Created by Christian Varriale on 20/03/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import UIKit
import Vision

class ViewController: UIViewController {
    
    //Definizione di un tipo
    typealias ModelTuple = (model: MLModel, modelName: String, outputLabel: UILabel)
    
    // MARK: Constants
    private enum Constant {
        static let outputFormat = "%@: %d%% - %@"
        static let performingClassificationFormat = "%@ is performing classification..."
    }
    
    // MARK: Private Variables & IBOutlet
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var mobileNetLabel: UILabel!
    @IBOutlet private weak var squeezeNetLabel: UILabel!
    @IBOutlet private weak var resNet50Label: UILabel!
    @IBOutlet private weak var inceptionV3Label: UILabel!
    @IBOutlet private weak var vgg16Label: UILabel!
    
    //Queste variabili lazy vengono create solo quando la funzione viene richiamata.
    lazy private var modelTuples: [ModelTuple] = {
       return [
        (MobileNet().model, "MobileNet", mobileNetLabel),
        (Inceptionv3().model, "InceptionV3", inceptionV3Label),
        (Resnet50().model, "ResNet50", resNet50Label),
        (VGG16().model, "VGG16", vgg16Label),
        (SqueezeNet().model, "SqueezeNet", squeezeNetLabel)
        ]
    }()
    
    // MARK: Button Actions
    @IBAction func pickImageButtonPressed(_ sender: Any) {
        //tipologia di alert, che contiene
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //la camera
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { _ in
            DispatchQueue.main.async {
                self.presentImagePicker(withType: .camera)
            }
        }
        
        //la galleria delle fotografie
        let libraryAction = UIAlertAction(title: "Photo Library", style: .default) { _ in
            DispatchQueue.main.async {
                self.presentImagePicker(withType: .photoLibrary)
            }
        }
        
        //infine il tasto cancel
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        //aggiunta delle azioni al controller
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(libraryAction)
        actionSheet.addAction(cancelAction)
        
        //Show della view
        present(actionSheet, animated: true, completion: nil)
    }
    
    // MARK: Private Methods
    
    private func presentImagePicker(withType type: UIImagePickerController.SourceType) {
        //definizione dell'imagePicker per camera e galleria
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.sourceType = type
        present(pickerController, animated: true)
    }
    
    private func detectObjects(from image: CIImage) {
        //per ognuno di quei modelli
        for tuple in modelTuples {
            //indico quale sto utilizzando
            tuple.outputLabel.text = String(format: Constant.performingClassificationFormat, tuple.modelName)
            
            //creo il modello con vision
            guard let visionModel = try? VNCoreMLModel(for: tuple.model) else {
                fatalError("Can't load model")
            }
            
            //faccio la richiesta, prendendomi il primo elemento
            let visionRequest = VNCoreMLRequest(model: visionModel) { (request, error) in
                guard let results = request.results as? [VNClassificationObservation],
                    let topResult = results.first else {
                        fatalError("Unexpected result type from VNCoreMLRequest")
                }
                
                //in modo asincrono, lasciando il thread principale, vado a mostrare il nome con relativa confidence
                DispatchQueue.main.async {
                    tuple.outputLabel.text = String(format: Constant.outputFormat, tuple.modelName, Int(topResult.confidence * 100), topResult.identifier)
                }
            }
            
            //gestione dell'immagine da parte dell'handler
            let handler = VNImageRequestHandler(ciImage: image)
            DispatchQueue.global(qos: .userInteractive).async {
                do {
                    try handler.perform([visionRequest])
                } catch {
                    print(error)
                }
            }
        }
        
    }
}

// MARK: - UINavigationControllerDelegate + UIImagePickerControllerDelegate
extension ViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    //carico immagine nel picker e la converto
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        dismiss(animated: true)
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            fatalError("Couldn't load image picked image")
        }
        
        imageView.image = image
        
        guard let ciImage = CIImage(image: image) else {
            fatalError("Couldn't convert UIImage to CIImage")
        }
        
        detectObjects(from: ciImage)
    }
    
}


